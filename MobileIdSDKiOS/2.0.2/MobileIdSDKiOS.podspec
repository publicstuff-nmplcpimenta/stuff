Pod::Spec.new do |s|
  s.name            = 'MobileIdSDKiOS'
  s.version         = '2.0.2'
  s.summary         = 'MobileID Software Development Kit'

  s.description     = <<-DESC
    MobileID SDK providing an API for the MobileID wallet services, platform, components for MRZ reading, NFC eDocument
    handling and Biometric face capture/evaluation.
  DESC

  s.homepage        = 'http://www.vision-box.com'
  s.license         = { type: 'MobileID SDK License Agreement', file: 'LICENSE.md' }
  s.author          = { 'VISION-BOX, S.A' => 'mobile.apps@vision-box.com' }

  s.source          = { http: 'https://gitlab.com/publicstuff-nmplcpimenta/stuff/-/raw/master/frameworks/MobileIdSDKiOS-' + s.version.to_s + '.zip' }

  s.ios.vendored_frameworks = 'MobileIdSDKiOS.framework'

  s.ios.deployment_target = '10.0'

  s.swift_versions = ['5']

  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  s.dependency 'Sentry', '4.4.1'
  s.dependency 'lottie-ios', '~> 3.1.9'
  s.dependency 'DocumentReader', '~> 5.3.1509'
  s.dependency 'DocumentReaderMRZRFID', '~> 5.3.3491'
  s.dependency 'Swinject', '~> 2.7.1'
  s.dependency 'Alamofire', '~> 4.9.1'
  s.dependency 'AlamofireNetworkActivityLogger', '~> 2.4.0'
  s.dependency 'p2.OAuth2', '~> 5.0.0'
end
